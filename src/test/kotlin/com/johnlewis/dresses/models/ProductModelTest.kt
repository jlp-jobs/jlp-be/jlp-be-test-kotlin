package com.johnlewis.dresses.models

import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNull
import com.johnlewis.dresses.domain.Price
import com.johnlewis.dresses.domain.Product
import org.junit.jupiter.api.Test

internal class ProductModelTest {
    @Test
    fun `should populate from domain with all fields specified`() {
        val domain = Product(
            productId = "productId1",
            title = "title1",
            price = Price(now = "nowPrice1", was = "wasPrice1", then = "thenPrice1")
        )

        val result = ProductModel.from(domain)

        assertThat(result.productId).isEqualTo("productId1")
        assertThat(result.title).isEqualTo("title1")
        assertThat(result.price.now).isEqualTo("nowPrice1")
        assertThat(result.price.was).isEqualTo("wasPrice1")
        assertThat(result.price.then).isEqualTo("thenPrice1")
    }

    @Test
    fun `should populate from domain with optional fields missing`() {
        val domain = Product(
            productId = "productId1",
            title = "title1",
            price = Price(now = "nowPrice1", was = null, then = null)
        )

        val result = ProductModel.from(domain)

        assertThat(result.price.now).isEqualTo("nowPrice1")
        assertThat(result.price.was).isNull()
        assertThat(result.price.then).isNull()
    }
}