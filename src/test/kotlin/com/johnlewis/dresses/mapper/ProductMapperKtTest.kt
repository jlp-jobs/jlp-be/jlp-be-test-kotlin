package com.johnlewis.dresses.mapper

import assertk.assertThat
import assertk.assertions.isEqualTo
import assertk.assertions.isNull
import com.johnlewis.dresses.repositories.UpstreamProduct
import com.johnlewis.dresses.repositories.UpstreamVariantPriceRange
import com.johnlewis.dresses.repositories.UpstreamVariantPriceRangeDisplay
import com.johnlewis.dresses.repositories.UpstreamVariantPriceRangeReductionHistory
import org.junit.jupiter.api.Test

internal class ProductMapperKtTest {
    @Test
    fun `should map product`() {
        val upstreamProduct = UpstreamProduct(
            productId = "productId1",
            title = "title1",
            variantPriceRange = UpstreamVariantPriceRange(
                UpstreamVariantPriceRangeDisplay("maxDisplayPrice"),
                emptyList()
            )
        )

        val result = mapProduct(upstreamProduct)

        assertThat(result.productId).isEqualTo("productId1")
        assertThat(result.title).isEqualTo("title1")
        assertThat(result.price.now).isEqualTo("maxDisplayPrice")
        assertThat(result.price.was).isNull()
        assertThat(result.price.then).isNull()
    }

    @Test
    fun `should populate was price with reduction with lowest chronology`() {
        val reductionHistory = listOf(
            UpstreamVariantPriceRangeReductionHistory(
                chronology = 3,
                display = UpstreamVariantPriceRangeDisplay("£3")
            ),
        )

        val upstreamProduct = UpstreamProduct(
            productId = "productId1",
            title = "title1",
            variantPriceRange = UpstreamVariantPriceRange(
                UpstreamVariantPriceRangeDisplay("maxDisplayPrice"),
                reductionHistory
            )
        )

        val result = mapProduct(upstreamProduct)

        assertThat(result.price.was).isEqualTo("£3")
        assertThat(result.price.then).isNull()
    }

    @Test
    fun `should populate then price with reduction with second lowest chronoloy`() {
        val reductionHistory = listOf(
            UpstreamVariantPriceRangeReductionHistory(
                chronology = 3,
                display = UpstreamVariantPriceRangeDisplay("£3")
            ),
            UpstreamVariantPriceRangeReductionHistory(
                chronology = 1,
                display = UpstreamVariantPriceRangeDisplay("£1")
            ),
            UpstreamVariantPriceRangeReductionHistory(
                chronology = 2,
                display = UpstreamVariantPriceRangeDisplay("£2")
            ),
        )

        val upstreamProduct = UpstreamProduct(
            productId = "productId1",
            title = "title1",
            variantPriceRange = UpstreamVariantPriceRange(
                UpstreamVariantPriceRangeDisplay("maxDisplayPrice"),
                reductionHistory
            )
        )

        val result = mapProduct(upstreamProduct)

        assertThat(result.price.was).isEqualTo("£1")
        assertThat(result.price.then).isEqualTo("£2")
    }
}