package com.johnlewis.dresses.functional

import assertk.assertThat
import assertk.assertions.isEqualTo
import com.johnlewis.dresses.configureRoutes
import org.http4k.cloudnative.env.Environment
import org.http4k.core.Method.GET
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status.Companion.OK
import org.http4k.format.Jackson.asJsonObject
import org.http4k.server.Jetty
import org.http4k.server.asServer
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class DressesFunctionalTest {
    private val mockExternalDressesServer = MockDressesServer()
    private val environment = Environment.from(
        mapOf(
            "PRODUCTS_API_BASE_URL" to "http://localhost:${mockExternalDressesServer.server.port()}/base-path",
        )
    )
    private val application = configureRoutes(environment)


    @BeforeEach
    fun setup() {
        mockExternalDressesServer.server.start()
    }

    @AfterEach
    fun teardown() {
        mockExternalDressesServer.server.stop()
    }

    @Test
    fun `should return dresses response`() {
        val request = Request(GET, "/products/dresses")

        val response = application(request)

        assertThat(response.status).isEqualTo(OK)
        val firstProductJson = response.bodyString().asJsonObject()[0]
        assertThat(firstProductJson["productId"].asText()).isEqualTo("6136180")
        assertThat(firstProductJson["title"].asText()).isEqualTo("John Lewis & Partners Baby Duck Jersey Dress, Light Green")
        assertThat(firstProductJson["price"]["now"].asText()).isEqualTo("£11.00")
        assertThat(firstProductJson["price"]["was"].asText()).isEqualTo("£9.00")
        assertThat(firstProductJson["price"]["then"].asText()).isEqualTo("£6.00")

        assertThat(mockExternalDressesServer.capturedRequest!!.uri.path).isEqualTo("/base-path/dresses")
    }

    private class MockDressesServer {
        var capturedRequest: Request? = null
        val server = { request: Request ->
            capturedRequest = request
            Response(OK).body(readResourceFile("external-dresses-api-response.json"))
        }.asServer(Jetty(1234))

        private fun readResourceFile(filename: String) = javaClass.classLoader.getResource(filename).readText()
    }
}