package com.johnlewis.dresses.service

import assertk.assertThat
import assertk.assertions.containsExactlyInAnyOrder
import com.johnlewis.dresses.domain.Price
import com.johnlewis.dresses.domain.Product
import com.johnlewis.dresses.models.PriceModel
import com.johnlewis.dresses.models.ProductModel
import com.johnlewis.dresses.repositories.ProductsRepository
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Test

internal class DressesRetrieverTest {
    @Test
    fun `should retrieve products and build models`() {
        val products = listOf(
            Product(
                productId = "productId1",
                title = "title1",
                price = Price("nowPrice1")
            ),
            Product(
                productId = "productId2",
                title = "title2",
                price = Price("nowPrice2")
            )
        )

        val mockRepository = mockk<ProductsRepository> {
            every { get() } returns products
        }

        val retriever = DressesRetriever(mockRepository)

        val result = retriever.retrieve()

        assertThat(result).containsExactlyInAnyOrder(
            ProductModel(
                productId = "productId1",
                title = "title1",
                price = PriceModel("nowPrice1")
            ),
            ProductModel(
                productId = "productId2",
                title = "title2",
                price = PriceModel("nowPrice2")
            ),
        )
    }
}