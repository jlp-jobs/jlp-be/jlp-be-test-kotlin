package com.johnlewis.dresses.repositories

import assertk.assertThat
import assertk.assertions.isEqualTo
import com.johnlewis.dresses.domain.Product
import com.johnlewis.dresses.mapper.ProductMapper
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.http4k.core.HttpHandler
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status.Companion.OK
import org.junit.jupiter.api.Test

internal class ProductsRepositoryTest {
    @Test
    fun `should make correct request to upstream service`() {
        var capturedRequest: Request? = null
        val client = { request: Request ->
            capturedRequest = request
            Response(OK).body("""{"products": []}""")
        }

        val productsRepository = ProductsRepository(
            client = client,
            productMapper = mockk()
        )

        productsRepository.get()

        assertThat(capturedRequest!!.uri.path).isEqualTo("/dresses")
        assertThat(capturedRequest!!.header("User-Agent")).isEqualTo("Back-end tech test")
    }

    @Test
    fun `should call product mapper with deserialised json and return mapped domain object`() {
        val mockProduct = mockk<Product>()
        val mockMapper = mockk<ProductMapper>()
        every { mockMapper(any()) } returns mockProduct

        val responseJson = """{
                    |  "products": [
                    |    {
                    |      "productId": "productId1",
                    |      "title": "title1",
                    |      "variantPriceRange": {
                    |        "display": {
                    |          "max": "£37"
                    |        },
                    |        "reductionHistory": [
                    |          {
                    |            "chronology": 0,
                    |            "display": {
                    |              "max": "£9.00",
                    |              "min": "£7.00"
                    |            }
                    |          },
                    |          {
                    |            "chronology": 1,
                    |            "display": {
                    |              "max": "£6.00",
                    |              "min": "£4.50"
                    |            }
                    |          }
                    |        ]
                    |      }
                    |    }
                    |  ]
                    |}""".trimMargin()

        val client: HttpHandler = { Response(OK).body(responseJson) }
        val productsRepository = ProductsRepository(
            client = client,
            productMapper = mockMapper
        )

        val result = productsRepository.get()

        verify {
            mockMapper(
                UpstreamProduct(
                    productId = "productId1",
                    title = "title1",
                    variantPriceRange = UpstreamVariantPriceRange(
                        display = UpstreamVariantPriceRangeDisplay("£37"),
                        reductionHistory = listOf(
                            UpstreamVariantPriceRangeReductionHistory(0, UpstreamVariantPriceRangeDisplay("£9.00")),
                            UpstreamVariantPriceRangeReductionHistory(1, UpstreamVariantPriceRangeDisplay("£6.00")),
                        )
                    )
                )
            )
        }

        assertThat(result.single()).isEqualTo(mockProduct)
    }
}