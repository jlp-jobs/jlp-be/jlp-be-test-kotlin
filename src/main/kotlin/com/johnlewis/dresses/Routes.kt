package com.johnlewis.dresses

import com.johnlewis.dresses.EnvironmentKeys.PRODUCTS_API_BASE_URL
import com.johnlewis.dresses.mapper.mapProduct
import com.johnlewis.dresses.repositories.ProductsRepository
import com.johnlewis.dresses.routes.dressesRoute
import com.johnlewis.dresses.service.DressesRetriever
import org.http4k.client.OkHttp
import org.http4k.cloudnative.env.Environment
import org.http4k.core.HttpHandler
import org.http4k.core.then
import org.http4k.filter.ClientFilters.SetBaseUriFrom
import org.http4k.routing.routes

fun configureRoutes(environment: Environment): HttpHandler {
    val dressesRetriever = DressesRetriever(
        ProductsRepository(
            client = SetBaseUriFrom(PRODUCTS_API_BASE_URL(environment)).then(OkHttp()),
            productMapper = ::mapProduct
        )
    )

    return routes(
        dressesRoute(dressesRetriever)
    )
}