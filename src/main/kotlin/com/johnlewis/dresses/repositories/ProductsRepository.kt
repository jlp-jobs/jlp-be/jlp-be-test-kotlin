package com.johnlewis.dresses.repositories

import com.johnlewis.dresses.domain.Product
import com.johnlewis.dresses.mapper.ProductMapper
import org.http4k.core.Body
import org.http4k.core.HttpHandler
import org.http4k.core.Method.GET
import org.http4k.core.Request
import org.http4k.format.Jackson.auto

class ProductsRepository(
    private val client: HttpHandler,
    private val productMapper: ProductMapper
) {
    private val formatUpstreamProducts = Body.auto<UpstreamProducts>().toLens()

    fun get(): List<Product> {
        val request = Request(GET, "/dresses")
            .header("User-Agent", "Back-end tech test")

        val response = client(request)

        return formatUpstreamProducts(response)
            .products
            .map { productMapper(it) }
    }
}

private data class UpstreamProducts(val products: List<UpstreamProduct>)
data class UpstreamProduct(
    val productId: String,
    val title: String,
    val variantPriceRange: UpstreamVariantPriceRange
)

data class UpstreamVariantPriceRange(
    val display: UpstreamVariantPriceRangeDisplay,
    val reductionHistory: List<UpstreamVariantPriceRangeReductionHistory>
)

data class UpstreamVariantPriceRangeDisplay(val max: String)
data class UpstreamVariantPriceRangeReductionHistory(val chronology: Int, val display: UpstreamVariantPriceRangeDisplay)