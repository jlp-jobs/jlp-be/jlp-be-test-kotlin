package com.johnlewis.dresses

import org.http4k.cloudnative.env.EnvironmentKey
import org.http4k.core.Uri

object EnvironmentKeys {
    val PRODUCTS_API_BASE_URL = EnvironmentKey.map({ Uri.of(it) }, Uri::toString)
        .defaulted(
            name = "PRODUCTS_API_BASE_URL",
            default = Uri.of("https://storage.googleapis.com/jl-backend-technical-assessment-service-stubs/api/search/keyword")
        )
}