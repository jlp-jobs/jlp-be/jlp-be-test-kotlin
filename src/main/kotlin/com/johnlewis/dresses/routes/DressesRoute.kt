package com.johnlewis.dresses.routes

import com.johnlewis.dresses.models.ProductModel
import com.johnlewis.dresses.service.DressesRetriever
import org.http4k.core.Body
import org.http4k.core.Method
import org.http4k.core.Response
import org.http4k.core.Status.Companion.OK
import org.http4k.core.with
import org.http4k.format.Jackson.auto
import org.http4k.routing.bind

fun dressesRoute(dressesRetriever: DressesRetriever) =
    "/products/dresses" bind Method.GET to {
        val dresses = dressesRetriever.retrieve()
        Response(OK).with(formatProductModels of dresses)
    }

private val formatProductModels = Body.auto<List<ProductModel>>().toLens()