package com.johnlewis.dresses.domain

data class Product(
    val productId: String,
    val title: String,
    val price: Price
)

data class Price(val now: String, val was: String? = null, val then: String? = null)