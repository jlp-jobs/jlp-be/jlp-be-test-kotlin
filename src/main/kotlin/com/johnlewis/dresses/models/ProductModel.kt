package com.johnlewis.dresses.models

import com.johnlewis.dresses.domain.Product

data class ProductModel(
    val productId: String,
    val title: String,
    val price: PriceModel
) {
    companion object {
        fun from(product: Product) = ProductModel(
            productId = product.productId,
            title = product.title,
            price = PriceModel(
                now = product.price.now,
                was = product.price.was,
                then = product.price.then
            )
        )
    }
}

data class PriceModel(
    val now: String,
    val was: String? = null,
    val then: String? = null
)