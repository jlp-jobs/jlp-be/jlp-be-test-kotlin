package com.johnlewis.dresses.mapper

import com.johnlewis.dresses.domain.Price
import com.johnlewis.dresses.domain.Product
import com.johnlewis.dresses.repositories.UpstreamProduct
import com.johnlewis.dresses.repositories.UpstreamVariantPriceRange

typealias ProductMapper = (UpstreamProduct) -> Product

fun mapProduct(upstreamProduct: UpstreamProduct) = Product(
    productId = upstreamProduct.productId,
    title = upstreamProduct.title,
    price = mapPrice(upstreamProduct.variantPriceRange)
)

private fun mapPrice(variantPriceRange: UpstreamVariantPriceRange): Price {
    val sortedReductionHistory = variantPriceRange.reductionHistory
        .sortedBy { it.chronology }
        .map { it.display.max }

    return Price(
        now = variantPriceRange.display.max,
        was = sortedReductionHistory.getOrNull(0),
        then = sortedReductionHistory.getOrNull(1)
    )
}
