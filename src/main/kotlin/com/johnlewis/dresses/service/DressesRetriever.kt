package com.johnlewis.dresses.service

import com.johnlewis.dresses.models.ProductModel
import com.johnlewis.dresses.repositories.ProductsRepository

class DressesRetriever(private val productsRepository: ProductsRepository) {
    fun retrieve(): List<ProductModel> {
        return productsRepository.get()
            .map { ProductModel.from(it) }
    }
}