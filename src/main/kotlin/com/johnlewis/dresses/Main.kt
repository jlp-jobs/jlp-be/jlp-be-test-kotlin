package com.johnlewis.dresses

import org.http4k.cloudnative.env.Environment.Companion.ENV
import org.http4k.cloudnative.env.Environment.Companion.JVM_PROPERTIES
import org.http4k.server.Jetty
import org.http4k.server.asServer

fun main() {
    configureRoutes(JVM_PROPERTIES overrides ENV)
        .asServer(Jetty())
        .start()
}