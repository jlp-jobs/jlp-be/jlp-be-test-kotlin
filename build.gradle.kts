plugins {
    kotlin("jvm") version "1.9.0"
    application
}

repositories {
    mavenCentral()
}

application {
    mainClass.set("com.johnlewis.dresses.MainKt")
}

dependencies {
    implementation(platform("org.http4k:http4k-bom:5.4.1.0"))
    implementation("org.http4k:http4k-core")
    implementation("org.http4k:http4k-server-jetty")
    implementation("org.http4k:http4k-format-jackson")
    implementation("org.http4k:http4k-cloudnative")
    implementation("org.http4k:http4k-client-okhttp")
    implementation("ch.qos.logback:logback-classic:1.4.8")


    testImplementation(kotlin("test"))
    testImplementation("org.junit.jupiter:junit-jupiter:5.10.0")
    testImplementation("com.willowtreeapps.assertk:assertk-jvm:0.26.1")
    testImplementation("io.mockk:mockk:1.13.5")
}

tasks.test {
    useJUnitPlatform()
}
